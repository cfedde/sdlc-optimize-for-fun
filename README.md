# SDLC Workflow

Slides for a quick presentation about team level SDLC at medium level
of detail. Focusing on how a team can do work and track what they are
doing.

## Slides

Find [slides](https://cfedde.gitlab.io/sdlc-optimize-for-fun/Slides.html)
at this url: https://cfedde.gitlab.io/sdlc-optimize-for-fun/Slides.html

To view them locally 

- Clone this repo
- Load `Slides.html` in a web browser

## Colophon

These slides are using [Marp](https://marp.app/) to translate markdown
into a standalone HTML slides file.

Steps were:
- Install the marp CLI tool from the releases site: https://github.com/marp-team/marp-cli/releases

The "think edit test" loop is supported by automatic slide generation:

- Write up the `Slides.md` file
  ```bash
  while true
  do 
      inotifywait Slides.md
      marp Slides.md
  done
  ```
  


