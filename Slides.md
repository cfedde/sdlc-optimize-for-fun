---
theme: gaia
paginate: true

---
# SDLC -- Optimizing for fun

Author: Chris Fedde
email: chris@fedde.us

---
# Topics

- Optimizing for fun
- SDLC
- Sharing the load
- Reliability
- Work Tracking
- Releases

---
# Optimizing for fun

* Some of us like our work 
* But there are parts of the work that are
  * Boring
  * Repetitive
  * Disruptive
* Those parts are "no fun"

---
# Optimizing for fun

* We can fix lots of the "no fun" stuff
* We work with computers
* The whole purpose of computers is to do the boring, repetitive stuff
* They can help us avoid the disruptive stuff too
* We just have to leverage them to help us
* That gives us more time for the fun stuff

---
# Optimizing for fun

* Implementing an SDLC will help realize the potential
* Make the computer do the mundane stuff
* Let us work on the cool stuff

---
# Optimizing for fun

* It's an iterative process 
* Reduce defects to improve performance and features over time
* An SDLC provides a feedback loop that enables 
  * process corrections 
  * at each iteration

---
# Optimizing for fun

* We can automate away all the boring, repetitive stuff
  * Build, maintain and use "Current Practice" documents
  * Write code to do the easy stuff
  * Automate our tests
  * Cross train
  * Document
  * Revise, refactor and improve the process
* Doing all this will make it easier and quicker to fix the disruptive
  stuff

---
# Optimizing for fun

* A well thought out SDLC processes will:
  * Give us time to do more stuff
  * Let us work on more cool stuff
  * Make it easier to recover from failures
  * Make our system more reliable
  * Make better use of all team Members

---
# Optimizing for fun

* An SDLC is a management process
* Not a specific tool
* Think of it as a set of principles, values
* Plus the practices and tooling to support them
* The point is to give thought to how our work
* Not just what we work on
* Adopt standard approaches to performing the work across the team

---
# Optimizing for fun

* Principles
  * Automate all the things
  * Rough consensus and running code
  * Write it down
* Values
  * People first
  * Respect for one another
  * Getting things done

---
# SDLC

* System Development Life Cycle 
* The important words are "Life" and "Cycle"
* Someone will keep it running as long as there are
  people who care about using it.
* Why make that job harder than it has to be?

---
# SDLC -- Goals

* Keep the service:
  * Available
  * Useful
  * Up to date
  * Secure
* All while taking less time overall

---
# SDLC -- Principles

* It's a laboratory. Treat it like one.
* Infrastructure as Code is __Code__
* Discover, Develop, Test, Deploy
* Publish your engineering notes in the work tracking issue
* Publish and maintain your processes
* Automate your processes
  * Including your development process

---
# SDLC -- It's a Lab

* Engineering is Applied Science
* Science is the practice of discovering what can be predicted
  * And how to predict it
* We build and maintain predictable processes

---
# SDLC -- It's a Lab

* "Laboratory" does not mean "Free For All"
  * It means carefully crafted experiments
  * Run with instrumentation
  * Recorded results
  * That can be repeated
  * Using documented procedures and automation

---
# SDLC --  It's Code

I'm sorry to have to tell you this:
* You are a programmer now.
* Not only do you have to do the thing
* But you also have to write code that does the thing
* And verify that your code does the thing correctly
* And automate that validation
* And write down why and how to use it
* And keep it working over time

---
# SDLC -- It's Code

* You can't do infrastructure as code without __code__
  * Learn about the reasons why we keep the system running
  * Also keep up with your vocational skills
  * Let programming be one of your skills
  * Development Cycle, IDE, Editors, Revision Control, Test Automation
  * Linux Command Line, Development Environments
  * Hardware, Containerization, Continuous Integration

---
# SDLC --  It's Code

* Programming languages (ansible, bash, python, etc)
* Programming tools (vs code, vi, linux cli, etc)
* Revision Control (git, GitLab, issues, projects, etc)
* Writing (engineering notes, procedures, user doc, markdown)
* Methodology ( [Agile](https://www.agilealliance.org/agile-essentials/), [TDD](https://www.agilealliance.org/glossary/tdd), [DoD](https://www.agilealliance.org/glossary/definition-of-done/), [DoR](https://www.agilealliance.org/glossary/definition-of-ready) )

---
# SDLC -- DDTD

* Proceed in four distinct steps:
  * Discover
  * Develop
  * Test
  * Deploy

---
# SDLC -- DDTD

* Discover
  * You cannot automate what you don't understand
  * Discover a way to get the thing done
  * Document it

---
# SDLC -- DDTD

* Develop and Test
  * On a feature branch
  * In a development environment
  * Write the code to do the process that was discovered 
  * Write test code to validate that the work is correct
  * Commit it to git and push it to GitLab
  * Configure CI to find and run your tests when pushed

---
# SDLC -- DDTD

* A special slide on testing
  * How do you convince yourself that your code does the right thing?
  * Write code that does that thing and shows success or failure
  * That's all there is to automated testing
  * Some tools that might help: pytest, bats, molecule
  * There are many more

---
# SDLC -- DDTD

* Deploy
  * Completed feature branches can be merged into main branch
  * They can be cherry picked into release branches
* Note:
  * An automated process
  * With a manual step in the middle
  * Is still a manual process

---
# SDLC -- Tool Stack:

GitLab for work tracking
* Issues
  * bug reports, feature requests, wish list
* kanban boards for meeting agenda
* Backlog
* Release plans
* Engineering notes

---
# SDLC -- Tool Stack:

GitLab for assets
* code 
* test suites
* configuration
* documentation,
* other original assets, images, public keys, 

---
# SDLC -- tool stack:

* keep secrets out of git
  * `$HOME/.config`
  * SaaS (ansible vault, hashicorp vault, bitwarden, etc) 

---
# SDLC -- tool stack:

* CI/CD
  * Continuous Integration (automated testing)
  * Continuous Deployment (when automated testing passes)

---
# SDLC -- tool stack:

* CI
  * On head of all branches
  * Use gitops (hooks) to trigger ci
  * Run your automated tests in ci
  * CI must not impact production

---
# SDLC -- Tool stack:

* CD
  * On release tags
  * When ci automated tests all pass
  * Initially manual
  * Eventually automatic

---
# SDLC -- tool stack:

* generally linux
* generally plain text files
* iron, virtualization & containers
* ansible first but not exclusively
* vscode
* vim, emacs, kate, etc
* anything else, keep a list

---
# SDLC -- Our revision control "flow"

How to use braches and release tags in gitlab:

* Development cycle:
  * Trunk with feature branch
* Production cycle: 
  * Release engineering branch with tagged releases
  * Cherry Pick into release engineering branch
* Fully ticketed
  * "Live Blog" your work
  * Merge requests are issues too

# SDLC -- Release Tags

* Consider using [Sematic Versions](https://semver.org/).
* Use the Release Tagging system in GitLab
* Learn about the [API](https://docs.gitlab.com/ee/api/) and
  [cli](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html) tools.

---
# SDLC -- Documentation

What needs to be documented?
* Why does the system exist? 
* Document in the repo `README.md` file
* Use markdown format
* Use urls and images where they are useful

---
# SDLC -- Documentation

Why does the system exist? 
* List the services the system provides
* Who the services are for

---
# SDLC -- documentation

Include notes about:
* Where to go for details
* Key contacts
* Availability

---
# SDLC -- documentation

* Sub directories can also have `README.md`
* Or not.
* Markdown files with other names can be useful too.
* We are not going to get into work logs and release notes here.

---
# SDLC -- Wrap Up

* System Development Life Cycle 
* It's all a lab. run experiments.
* Don't break production.
  * If you break production, "live blog" about it
  * Automate the failure away.
* Use the standard tools.
  * Lobby for better ones.
* Write stuff down.

---
# Sharing The Load

* The senior engineer cannot do everything
* There is always overhead
* There is always more to do
* Writing stuff down makes it real
* There is more than one way to do it
  * But try the current way first.

---
# Sharing The Load

* people like being trusted
* people make mistakes
* make it easy to do the right thing
* work to make it easy to recover from failure

---
# Sharing The Load

* Make it easy to hand off a task
* Solicit volunteers
* Make it easy to volunteer
* Use ticketing to track work
* Use pairing and mob to cross-train

---
# Reliability

* Is the system doing what we expect it to do?
* What did it do right?
* What did it do wrong?
* How can users initiate and escalate concerns?

---
# Reliability

Specific solutions are needed for each of these:

* Monitoring
* Notification
* Escalation
* Tracking

* These are _more_ important than the services the platform
delivers.

---
# Reliability

* We have to be able to find out that the system is delivering
  services correctly.
* Polling the users is a last resort.
* Real users know where you live.
* The tragedy is that most people will just go away without saying
  anything

---
# Work Tracking

* This Is Always The Hard Part
* It's not enough to just get it done
* You also need to record what you did.
  * In a github issue
  * With examples
  * And commands
* So others can learn from it.
* This is the key difference between technicians and engineers.

---
# Work Tracking

In engineering notes (issue comments) 
* Screenshots can be valuable, but:
* Text is better than images
    * Because you can copy-n-paste them
    * And search for them
* Images of text are better than nothing

---
# Releases

* All the assets we use have version numbers
* Our code should too
* Adopt [semantic versioning](https://semver.org/)
* Use the github release capability to tag important events.
* Deploy/deliver from release tags

---
# Releases

* Production servers have specific versions of releases installed
* There is a way to tell what release is installed on a system
* Establish and document rituals for putting new code/config into
  production
* When the rituals are documented they can be automated and refined

---
# Conclusion

* SDLC
  * All the technical bits
* Sharing the load
  * Community makes strong systems
* Reliability
  * Ability to sleep at night

---
# Conclusion

* Work tracking
  * You don't have to remember everything
* Releases
  * You can know what is running on each of your servers
* Optimize for fun
  * Teach the computers to do the boring stuff.
  * So you get to do the fun stuff.

---
# links

* [capability maturity model](https://en.wikipedia.org/wiki/capability_maturity_model)
* [site reliability engineering](https://www.redhat.com/en/topics/devops/what-is-sre)
* [gitlab flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
* [markdown presentation ecosystem](https://marp.app/)
* [chaos engineering](https://en.wikipedia.org/wiki/chaos_engineering)
* [immutable infrastructure](https://www.youtube.com/watch?v=ii4pfe9bbme)
* [cult of done manifesto](https://medium.com/@bre/the-cult-of-done-manifesto-724ca1c2ff13)
* [10 bullets](https://youtu.be/49p1jvlhuos) by tom sachs
* [12 factors](https://12factor.net/)

---
# SDLC -- optimizing for fun

- Questions?
- This [presentation](https://gitlab.com/cfedde/sdlc-optimize-for-fun/-/tree/main)
- Is on gitlab: 
  https://gitlab.com/cfedde/sdlc-optimize-for-fun/-/tree/main
